let http = require("http");

const port = 3000;

http.createServer((request, response) => {
	if(request.url == '/'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Server is successfully running");
	}

	if(request.url == '/login'){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("You are in the login page");
	}
		
	else{
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("Error");
	}
}).listen(port);
console.log(`server is successfully running`);